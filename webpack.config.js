var nodeExternals = require('webpack-node-externals');
var webpack = require('webpack');
var path = require('path');
var fs = require('fs');

/* helper function to get into build directory */
var distPath = function ( name ) {
  if ( undefined === name ) {
    return path.join('dist');
  }
  
  return path.join('dist', name);
};

module.exports = {
    entry: './src/index.ts',
    devtool: 'inline-source-map',
    target: 'node',
    output: {
        filename: distPath('main.js'),
        libraryTarget: "commonjs2"
    },
    resolve: {
        extensions: ['.ts', '.graphql'],
        modules: [
          'tests',
          'src',
        ]
      },
    plugins: [
        new webpack.IgnorePlugin(/vertx/),     
        new webpack.LoaderOptionsPlugin({
            options: {
                test: /\.ts$/,
                ts: {
                    compiler: 'typescript',
                    configFileName: 'tsconfig.json'
                },
                tslint: {
                    emitErrors: true,
                    failOnHint: true
                }
            }
        })
          
    ],
    module: {
        rules: [
            {                
                test: /\.ts?$/,
                exclude: /node_modules/,
                loaders: 'awesome-typescript-loader'
            },
            {
                test: /\.(graphql|gql)$/,
                exclude: /node_modules/,
                loader: 'graphql-tag/loader',
            },
        ]
    },
    externals: [nodeExternals()]
}
