const books = [
    {
      author: "J.K. Rowling",
      title: "Harry Potter and the Chamber of Secrets",
    },
    {
      author: "Michael Crichton",
      title: "Jurassic Park",
    },
];

export class BookService {
    constructor() {
        const db = "";
    }

    public create(parent, input, context, info) {
        const { title, author } = input;
        return books.push({ title, author });
    }

    public edit(parent, input, context, info) {
        const { id, title, author } = input;
        books[id].title = title;
        books[id].author = author;
        return books[id];
    }

    public delete(parent, id , context, info) {
        return null;
    }

    public findOne(parent, id, context, info) {
        return books[id];
    }

    public findMany(parent, input, context, info) {
        return books;
    }
}
