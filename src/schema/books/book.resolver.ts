import { PubSub } from "graphql-subscriptions";
import { BookService } from "./book.service";

const pubsub = new PubSub();
const service = new BookService();

// Resolvers define the technique for fetching the types in the
// schema.  We'll retrieve books from the "books" array above.
export default {
  Mutation: {
    addBook: (parent, input, context, info) => service.create(parent, input, context, info),
    editBook: (parent, input, context, info) => service.edit(parent, input, context, info),
  },
  Query: {
    book: (parent, { id }, context, info) => service.findOne(parent, id, context, info),
    books: (parent, input, context, info) => service.findMany(parent, input, context, info),
  },
  Subscription: {
    books: () => pubsub.asyncIterator("BOOK_CREATED"),
  },
};
