/**
 *
 * Resolvers for User
 *
 */

import { PubSub } from "graphql-subscriptions";
import { UserService } from "./user.service";

const pubsub = new PubSub();
const service = new UserService();

export default {
  Mutation: {
    addUser: (parent, input, context, info) => service.create(parent, input, context, info),
    editUser: (parent, input, context, info) => service.edit(parent, input, context, info),
  },
  Query: {
    user: (parent, { id }, context, info) => service.findOne(parent, id, context, info),
    users: (parent, input, context, info) => service.findMany(parent, input, context, info),
  },
  Subscription: {
    user: () => pubsub.asyncIterator("USER_CREATED"),
  },
};
