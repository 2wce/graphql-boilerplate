const user = [
    {
      author: "J.K. Rowling",
      title: "Harry Potter and the Chamber of Secrets",
    },
    {
      author: "Michael Crichton",
      title: "Jurassic Park",
    },
];

export class UserService {
    constructor() {
        const db = "";
    }

    public create(parent, input, context, info) {
        const { title, author } = input;
        return user.push({ title, author });
    }

    public edit(parent, input, context, info) {
        const { id, title, author } = input;
        user[id].title = title;
        user[id].author = author;
        return user[id];
    }

    public delete(parent, id , context, info) {
        return null;
    }

    public findOne(parent, id, context, info) {
        return user[id];
    }

    public findMany(parent, input, context, info) {
        return user;
    }
}
